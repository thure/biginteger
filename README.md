### What is this repository for?

*   **Quick summary**

    Langzahlarithmetik. Benutzt characters für die Ziffern, d.h. es
    wird zur Basis 256 gerechnet. Hinsichtlich des Funktionsumfanges,
    der über die Grundrechenarten hinausgeht, bin ich offen für
    Vorschläge.  Fermatscher Primzahltest etc....

    **Nichttriviale Algorithmen können (gern in Gedichtform erläutert)
      werden, siehe z.B. in der Datei
      [`compare.c`](https://bitbucket.org/thure/biginteger/raw/db874e8b2fd9459042f49d57f360d54beb7d4c74/biginteger/library/src/compare.c
      "in der Funktion BigInteger_Compare_Helper").**


    
*   **Version**  
    0.1 alpha
    

### How do I get set up?

*   **Summary of set up**

        #!/bin/bash
        
        cd /path/to/awesome/projects/
        
        git clone git@bitbucket.org:thure/biginteger.git
        cd biginteger/biginteger/
        
        make
        
        cd tests/bin/
        ./test1.out
    

*   **Configuration**  
    Quasi nicht existent. Noch ist das Projekt klein genug.
    

*   **Dependencies**  
    Die Build-Umgebung ist Unix-lastig.  
    [`bash`](https://packages.debian.org/stable/bash
      "Bourne Again Shell"), [`make`](https://packages.debian.org/stable/make
      "GNU make") und ein ISO-C99-Compiler reichen im Wesentlichen aus,
    [`valgrind`](https://packages.debian.org/stable/valgrind
      "Valgrind: Fehleranalyse von Programmen zur Laufzeit") ist aber auch nicht verkehrt. Zur Performance-Analyse ist auch [`gprof`](https://packages.debian.org/stable/binutils
      "GNU profiler") denkbar.
    Wer unter einem Nicht-Unix-System arbeitet, muss das Makefile
    kopieren und anpassen und unter einem anderen Namen
    (z.B. `Makefile.windows`) abspeichern. Oder auf das Makefile
    verzichten.
    Die Kommandozeilenoptionen im Makefile sind auf den [`gcc`](https://packages.debian.org/stable/gcc
      "GNU C Compiler") abgestimmt.
    

*   **How to run tests**  
    `cd bin/tests`
    

*   **Deployment instructions**  
    So weit sind wir noch nicht :)  
    Ein einfaches `make` tut's.
    


### Contribution guidelines

*   **Writing modules**

    Man suche sich einen klar umrissenen Problembereich
    (z.B. *Primzahltests*), spreche Art und Umfang mit mir ab,
    erstelle im Verzeichnis `library/src` ein passendes Dateipaar
    (z.B. `primes.c` und `primes.h`) und benutze das folgende
    Grundgerüst in der Datei `primes.h`:
      
        /*
          Primzahltests
        */
        
        
        #include "primes.h"
        #include <stdlib.h>
        #include <stdio.h>
        #include "datatypes.h"
        
        
        boolean isPrime (BigInteger b);
        
        ...
        ...
        ...


    sowie das folgende Grundgerüst in der Datei `primes.c`:
      
        /*
          Primzahltests
        */
        
        #ifndef BIGINT_PRIMES_H
        
        #define BIGINT_PRIMES_H
        
        #include "primes.h"    /* the first include must be the corresponding header file */
        #include "datatypes.h"
        #include <stdio.h>
        
        
        boolean BigInteger_isPrime (BigInteger b) {
          
          if (! BigInteger_isValid (b)) {
            BOOM ("BigInteger_isPrime: Invalid BigInteger\n");
            return false;
          }
        
          /* not yet implemented! */
        
          return false;
          
        }

        ...
        ...
        ...

        #endif
          
     
          
     

    In der `.c`-Datei ist darauf zu achten, die korrespondierende
    `.h`-Datei als erste zu `#include`n. Danach kommen `stdlib.h` und
    `stdio.h`, danach weitere Header der vorliegenden Bibliothek. Wenn
    alle Funktionen wie vorgesehen funktionieren, sind alle nicht
    benötigen includes zu entfernen.
    Im Übrigen nehme man sich die Dateien [`addsub.h`](https://bitbucket.org/thure/biginteger/raw/bdf7c058a728db0578f2db18fc81cddcde182ab0/biginteger/library/src/addsub.h
      "man denke auch an die Kommentare, die den Zweck der Funktion beschreiben")
 und [`addsub.c`](https://bitbucket.org/thure/biginteger/raw/bdf7c058a728db0578f2db18fc81cddcde182ab0/biginteger/library/src/addsub.c
      "man denke auch an die Kommentare, die den Zweck der Funktion beschreiben") zum Vorbild.  

      Wann immer möglich (m.a.W. *so gut wie immer*, insbesondere auch
      bei der Implementierung der Bibliotheksfunktionen) benutze man
      zum Setzen einer Ziffer eines BigIntegers nicht den direkten
      Zugriff auf das Array `b->digits[i] = d`, sondern den Aufruf
      `BigInteger_SetDigit (b, i, d)`. Dies stellt sicher, dass nicht
      über die Arraygrenzen hinaus geschrieben wird und dass im Array
      nur Ziffern abgelegt werden können. Der Overhead bei der
      Benutzung dieser Funktion ist dadurch gerechtfertigt, nicht bei
      jedem Zugriff auf einen BigInteger das Ziffernarray auf
      Vorhandensein von Nichtziffern zu untersuchen.
      Weiteres im Kommentar in der Datei [`datatypes.c`](https://bitbucket.org/thure/biginteger/raw/b2b3af91a70afd4e5abf567f1ffc78b11b0ddee8/biginteger/library/src/datatypes.c
      "man denke auch an die Kommentare, die den Zweck der Funktion beschreiben").

      Es ist im Makefile außerdem eine Zeile der Form  
    `libs += $(lbin)/primes.o`  
    zu ergänzen.

      Man vergesse nicht, aussagekräftige Tests der neu implementierten Funktionen zu schreiben.

*   **Writing tests**  
    Man nehme sich die Dateien [`comparetest.c`](https://bitbucket.org/thure/biginteger/raw/869e3e036cff6cc05e45e0ab9b503649321d2a7a/biginteger/tests/src/comparetest.c
      "In Testdateien sind Kommentare nicht sonderlich wichtig, es sei denn, es passiert etwas Merkwürdiges") oder [`CopyValueTest.c`](https://bitbucket.org/thure/biginteger/raw/af190a51e331a39a4a9283bd23eb528ad1f0cc8b/biginteger/tests/src/CopyValueTest.c
      "In Testdateien sind Kommentare nicht sonderlich wichtig, es sei denn, es passiert etwas Merkwürdiges") zum Vorbild.
    Es ist im Makefile außerdem eine Zeile der Form  
    `execs += $(ebin)/awesomeTest.out`  
    zu ergänzen.
    

* **Code review**  
    Ist nie verkehrt. Wer etwas Verbesserungswürdiges erkennt, benutze
    die Kommentarfunktionen von bitbucket oder schrei(b)e mir eine
    E-Mail, in deren Betreff die Zeichenfolge `[bigint]` enthalten
    ist. Wer sich für das Schreien entscheidet, achte darauf, RFC-5322-konform
    zu schreien.  
    
     * Wie kann man den Code in den `switch`-Anweisungen in der Datei [`datatypes.c`](https://bitbucket.org/thure/biginteger/src/b74da3fe986cd18ebab9994d71d9df06ea93b876/biginteger/library/src/datatypes.c "") ab Zeile 137 bzw. ab Zeile 377 schön refactoren?  

*   **Documentation**  
    Zunächst orientiere man sich an den bereits vorhandenen
    Dateien. Sobald ich die Muße (und Mitstreiter :) dazu habe, gibt
    es auch Doku, vermutlich in LaTeX-Form.

*   **Other guidelines**  
    Man fühle sich berufen, meinen Programmierstil zu
    imitieren oder Änderungen daran mit mir zu diskutieren.

    Außerdem ist die Semantik der Datentypen zu
    beachten. Beispielsweise sind sowohl `DigitPosition` als auch
    `NumberLength` Synonyme für `long int`: Wo die *Position* einer
    Ziffer gebraucht wird, benutze man `DigitPosition`; wo die
    *Anzahl* der Ziffern von Bedeutung ist, benutze man
    `NumberLength`, wo der *Wert* eines `long int` ausschlaggebend ist
    (d.h. die Zahl an sich), benutze man `long int`.

### Who do I talk to?

Thure Dührsen  
`T.Duehrsen@gmx.net`
