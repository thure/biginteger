#!/bin/bash

usage() {

    echo -e "\nUsage: $0 (cleanexe|cleanlib|clean|pristine)"

}

if [ 1 -ne "$#" ]
then
    usage
    exit 1
fi

case "$1" in
    cleanexe)
        for file in $(find . -iname "*.out"); do rm -f "$file"; done
        ;;

    cleanlib)
        for file in $(find . -iname "*.o"); do rm -f "$file"; done
        for file in $(find . -iname "*.gch"); do rm -f "$file"; done
        ;;

    clean)
        "$0" cleanlib
        "$0" cleanexe
        ;;

    pristine)
        "$0" clean
        for file in $(find . -iname "*~"); do rm -fv "$file"; done
        ;;

    *)
        usage
        exit 1
        ;;
esac
