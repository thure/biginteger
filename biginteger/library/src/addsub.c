/*
  Addition und Subtraktion
  Zeitmessung ungefähr so?   http://codepad.org/q8bvQQyb
*/

#include "addsub.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h> /* für memset */
#include "datatypes.h"
#include "compare.h"
#include "ioterm.h"


int BI_SumDigitsMod10_slow (BigInteger b) {

  DigitPosition i = -2;
  int result = 0;

  if (! BI_isValid (b)) {
    BOOM ("Invalid BigInteger\n");
    return 0;
  }

  for (i = b->length-1; i >= 0; i--) {
    result = (result + b->digits[i]) % 10;
  }
  return result;
}


int BI_SumDigitsMod10_fast (BigInteger b) {

  DigitPosition i = -2;
  int result = 0;

  if (! BI_isValid (b)) {
    BOOM ("Invalid BigInteger\n");
    return 0;
  }

  for (i = b->length-1; i >= 0; i--) {
    result += b->digits[i] + (result > 9 ? -10 : 0);
  }
  return result;
}


void BI_ChangeSign (BigInteger b) {

  if (! BI_isValid (b)) {
    BOOM ("Invalid BigInteger\n");
    return;
  }

  if (b->sign == '+') {
    b->sign = '-';
  } else {
    b->sign = '+';
  }
}


int BI_CarryRippleAdd (BigInteger s,
                               BigInteger a,
                               BigInteger b) {

  /* behandelt nur positive Zahlen.
     Alle anderen Fälle werden in der Funktion BI_Add
     auf diesen zurückgeführt.
     Das Zehnerkomplement ist auch eine positive Zahl!
  */

  /*
    Zur Sicherungsstelle siehe
    https://www.informatik.uni-kiel.de/uploads/media/DS1-Skript.pdf
    2015-05-29
  */

  Digit si          =  0;             /* Aktuelle Ergebnisziffer        */
  int   sum         =  0;             /* Summe der aktuellen Ziffern    */
  char  carry       =  0;             /* Übertrag                       */
  DigitPosition i   = -2;             /* Schleifenzähler                */
  NumberLength  max =  0;             /* maximale Länge der BigIntegers */
  DigitPosition guardDigitPos = -2;   /* Sicherungsstelle               */
  DigitPosition msbPos = -2;          /* Signifikanteste Stelle         */

  if (! BI_isValid (s)) {
    BOOM ("sum is invalid\n");
    return 2;
  }

  if (! BI_isValid (a)) {
    BOOM ("addend is invalid\n");
    return 2;
  }

  if (! BI_isValid (b)) {
    BOOM ("augend is invalid\n");
    return 2;
  }

  /* Zahlen normalisieren, damit max korrekt gesetzt werden kann */
  
  BI_Canonicalise (a);
  BI_Canonicalise (b);
  
  /*
    s auf Null setzen, sonst stehen da noch alte Ziffern drin!
    Beispiel: Add (11992940, 88007060) gefolgt von Add (528101, 471899)
  */
  BI_SetDecStringValue (s, "0");
  
  max = a->effLength;
  if (b->effLength > max) {
    max = b->effLength;
  }
  max++; /* Summe kann eine weitere Stelle haben */
  max++; /* Sicherungsstelle */

  if (a->length < max) {
    if (! BI_ChangeLength (a, max)) {
      BOOM ("changeLength failed for the addend\n");
      return CRADD_CANTDO;
    }
  }

  if (b->length < max) {

    if (! BI_ChangeLength (b, max)) {
      BOOM ("changeLength failed for the augend\n");
      return CRADD_CANTDO;
    }
  }

  if (s->length < max) {
    if (! BI_ChangeLength (s, max)) {
      BOOM ("changeLength failed for the sum\n");
      return CRADD_CANTDO;
    }
  }

  /* printf ("a = "); BI_PrintlnAllDigits (a); */
  /* printf ("b = "); BI_PrintlnAllDigits (b); */
  /* printf ("s = "); BI_PrintlnAllDigits (s); */
  /* printf ("\n"); */

  for (i = 0; i < a->length; i++) {

    Digit ai = a->digits[i];
    Digit bi = b->digits[i];

    sum = ai + bi + carry;

    si  = sum - (sum >= 0x100 ? 0x100 : 0);
    
    carry = (sum >= 0x100 ? 1 : 0);

#ifdef DEBUG
    printf ("a[%ld] = %3x, b[%ld] = %3x, s[%ld] = %3x, carry = %d\n",
            i, ai, i, bi, i, si, carry);
#endif

    BI_SetDigit (s, i, si);
    
  }

  /* Überlauf aufgetreten? */

  guardDigitPos = s->length-1;
  msbPos        = s->length-2;

  if ((BI_GetDigit (s, guardDigitPos) == 0) &&
      (BI_GetDigit (s, msbPos)        == 1)) {    
    return CRADD_OVERFLOW;
  }
  return CRADD_OK;
}


BigInteger BI_TensComplement (BigInteger A) {
	
  BigInteger result;
  int i = 0;
  boolean signChanged = false;

  if (! BI_isValid (A)) {
    BOOM ("Invalid BigInteger\n");
    return INVALID;
  }

  if (BI_isNegative (A)) {
    BI_ChangeSign (A);
    /*
      Die Funktion BI_Abs hat lineare Laufzeit in der
      Anzahl der Ziffern.
      Das Vorzeichen temporär zu drehen geht in O(1).
      Damit ist diese Funktion nicht threadsicher.
    */
    signChanged = true;
  }

  BI_Canonicalise (A);
  result = BI_Create (A->length);
    
  result -> sign = '+';
  BI_ChangeLength (result, A->length);

  for (i = 0; i < A->length; i++) {
    BI_SetDigit (result, i, 9-A->digits[i]);
  }
  BI_Inc (result);

  if (signChanged) {
    BI_ChangeSign (A);
  }
#ifdef DEBUG
  printf ("Das Zehnerkomplement von ");
  BI_PrintRaw (A);
  printf (" ist ");
  BI_PrintlnRaw (result);  
#endif
  result->isAComplement = true;
  return result;
}


void BI_TensComplementInPlace (BigInteger A) {

  A = A;
  BOOM ("TODO");

}


boolean BI_Add (BigInteger sum, BigInteger a, BigInteger b) {

  boolean result = false;
  BigInteger aClone = INVALID;
  BigInteger bClone = INVALID;
  BigInteger bTensComp = INVALID;
  BigInteger sumTensComp = INVALID;
  char sign = '\\';

  /* Wenn sum == a oder sum == b ist, fliegen die Zeiger wild durcheinander.
     Also werden die Summanden bei Bedarf geklont. Das ist zwar sehr
     ineffizient, aber es funktioniert.
  */

  if (BI_isIdentical (sum, a)) {
    aClone = BI_Clone (a);
    result = BI_Add (sum, aClone, b);
    BI_Destroy (aClone);
    goto finish;
  }

  if (BI_isIdentical (sum, b)) {
    bClone = BI_Clone (b);
    result = BI_Add (sum, a, bClone);
    BI_Destroy (bClone);
    goto finish;
  }

  /*
    Vorzeichen der Summanden behandeln
  */

  /* B */
  if (BI_isNonNegative (a) && BI_isNegative (b)) {
    bTensComp = BI_TensComplement (b);
    if (BI_isGreaterOrEqual(a, BI_Abs(b))) {
      sign = '+';
    } else {
      sign = '-';
    }
    
    result = BI_Add (sum, a, bTensComp);
    BI_Destroy (bTensComp);
    
    if (sign == '-') {
      sumTensComp = BI_TensComplement (sum);
      BI_CopyValue (sumTensComp, sum);
      BI_Destroy (sumTensComp);
      /* das muss auch in-place gehen */
    }
    sum->sign = sign;
    BI_Canonicalise (sum);
    if (result == CRADD_OVERFLOW) {
      /* Überlauf bei der Addition von a und ZK(b)
         die führende 1 ist zu streichen
      */
      fprintf (stderr, "Überlauf bei ZK (ist gewollt)\n");
      BI_SetDigit (sum, sum->length-1, 0);
    }
    goto finish;
  }

  /* C */
  if (BI_isNegative (a) && BI_isNonNegative (b)) {
    /* Zurückführen auf Fall B */
    BI_ChangeSign (a);
    BI_ChangeSign (b);
    result = BI_Add (sum, a, b);
    BI_ChangeSign (sum);
    BI_ChangeSign (b);
    BI_ChangeSign (a);
    goto finish;
  }

  /* D */
  if (BI_isNegative (a) && BI_isNegative (b)) {
    /* Zurückführen auf Fall A */
    BI_ChangeSign (a);
    BI_ChangeSign (b);
    result = BI_Add (sum, a, b);
    BI_ChangeSign (sum);
    BI_ChangeSign (b);
    BI_ChangeSign (a);
    goto finish;
  }
  
  /* A */
  /* Der Standardfall: sum, a, b sind drei verschiedene positive BigIntegers
     Wenn eine andere Implementierung der Addition genutzt werden
     soll, so ist dies nur hier festzulegen.
  */
  result = BI_CarryRippleAdd (sum, a, b);

 finish:
  BI_Canonicalise (a);
  BI_Canonicalise (b);
  BI_Canonicalise (sum);

  switch (result) {

  case CRADD_OK:
    return ADD_OK;
    break;

  case CRADD_OVERFLOW:
    return ADD_OVERFLOW;
    break;

  case CRADD_CANTDO:
    return ADD_CANTDO;
    break;

  default:
    BOOM ("Addition kaputt");
    return -42;
    break;
  }
}


boolean BI_AddTo (BigInteger b, BigInteger inc) {

  if (! BI_isValid (b)) {
    BOOM ("Invalid BigInteger\n");
    return false;
  }

  if (! BI_isValid (inc)) {
    BOOM ("Invalid BigInteger\n");
    return false;
  }

  return BI_Add (b, b, inc);

}


boolean BI_SmallIncrement (BigInteger b, int inc) {
  
  if (! BI_isValid (b)) {
    BOOM ("Invalid BigInteger\n");
    return false;
  }

  BigInteger binc = BI_Create (1);
  BI_SetSmallValue (binc, inc);
  return BI_AddTo (b, binc);

}


boolean BI_Inc (BigInteger b) {
  
  if (! BI_isValid (b)) {
    BOOM ("Invalid BigInteger\n");
    return false;
  }

  return BI_SmallIncrement (b, 1);
}


boolean BI_Dec (BigInteger b) {

  /* TODO: Testen, sobald alle Fälle der Addition funktionieren. */
  
  if (! BI_isValid (b)) {
    BOOM ("Invalid BigInteger\n");
    return false;
  }

  BigInteger binc = BI_Create (1);
  BI_SetSmallValue (binc, -1);
  return BI_AddTo (b, binc);
}


boolean BI_Sub (BigInteger diff,
                        BigInteger minuend,
                        BigInteger subtrahend) {

  diff = diff;
  minuend = minuend;
  subtrahend = subtrahend;
  BOOM ("TODO");

  return false;

}
