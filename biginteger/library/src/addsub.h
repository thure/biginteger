/*
  Addition und Subtraktion
 */


#ifndef BIGINT_ADDSUB_H

#define BIGINT_ADDSUB_H

#include "datatypes.h"


/*
  Berechnet die Quersumme modulo 10 mittels Modulo-Operator.
 */
int BI_SumDigitsMod10_slow (BigInteger b);


/*
  Berechnet die Quersumme modulo 10 mittels Subtraktion.
 */
int BI_SumDigitsMod10_fast (BigInteger b);


/*
  Kehrt das Vorzeichen des BigIntegers um.
 */
void BI_ChangeSign (BigInteger b);


/*
  Gibt an, ob die Carry-Ripple-Addition erfolgreich war.
 */
typedef enum { CRADD_OK = 0, CRADD_OVERFLOW, CRADD_CANTDO } CarryRippleAddResult;

/*
  Gibt an, ob die Addition erfolgreich war.
 */
typedef enum { ADD_OK = 0, ADD_OVERFLOW, ADD_CANTDO } AdditionResult;


/*
  Addiert zwei positive BigIntegers a und b nach der Schulmethode
  und speichert das Ergebnis im BigInteger sum.

  Rückgabewert: ADD_OK        bei erfolgreicher Addition ohne Überlauf
                ADD_OVERFLOW  bei erfolgreicher Addition mit Überlauf
                ADD_CANTDO    bei Nichtdurchführbarkeit
 */
int BI_CarryRippleAdd (BigInteger sum,
                               BigInteger a,
                               BigInteger b);


/*
 Berechnet das Zehnerkomplement eines BigIntegers.
 */
BigInteger BI_TensComplement (BigInteger A);


/*
  Überschreibt einen BigInteger mit seinem Zehnerkomplement.
  Ist bei einem Fall der Addition sinnvoll.
 */
void BI_TensComplementInPlace (BigInteger A);


/*
  Addiert zwei BigIntegers a und b und speichert das Ergebnis
  im BigInteger sum.

  Diese Funktion führt keine Plausibilitätsprüfungen durch, denn sie
  soll ausschließlich dazu dienen, eine Implementierung der Addition
  auszuwählen.
 */
boolean BI_Add (BigInteger sum, BigInteger a, BigInteger b);


/*
  Erhöht den Wert von b um den Wert von inc.
*/
boolean BI_AddTo (BigInteger b, BigInteger inc);

/*
  Erhöht den Wert von b um den Wert von inc.
*/
boolean BI_SmallIncrement (BigInteger b, int inc);


/*
  Erhöht den Wert von b um 1.
*/
boolean BI_Inc (BigInteger b);


/*
  Vermindert den Wert von b um 1.
*/
boolean BI_Dec (BigInteger b);


/*
  Subtrahiert subtrahend von minuend und schreibt das Ergebnis in diff.
*/
boolean BI_Sub (BigInteger diff,
                        BigInteger minuend,
                        BigInteger subtrahend);


#endif
