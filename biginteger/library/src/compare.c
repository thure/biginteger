/*
  Vergleichsoperationen
*/


#include "compare.h"
#include "datatypes.h"
#include "addsub.h"
#include "ioterm.h"    /* debug only? */
#include <stdlib.h>    /* debug only? */
#include <stdio.h>     /* debug only? */


static int BI_Compare_Helper (BigInteger u,
                                      BigInteger v,
                                      boolean signsChanged);

int BI_Compare (BigInteger u, BigInteger v) {

  return BI_Compare_Helper (u, v, false);

}


static int BI_Compare_Helper (BigInteger u,
                                      BigInteger v,
                                      boolean signsChanged) {

  DigitPosition i = -2;

  if (! BI_isValid (u)) {
    BOOM ("First argument is invalid\n");
    return -2;
  }

  if (! BI_isValid (v)) {
    BOOM ("Second argument is invalid\n");
    return -2;
  }

  BI_Canonicalise (u);
  BI_Canonicalise (v);

  if (u->sign == '+' && v->sign == '-') {
    return +1;
  }

  if (u->sign == '-' && v->sign == '+') {
    return -1;
  }

  if (u->sign == '-') {
    /* beide Zahlen sind negativ */

    int result = -3;

    BI_ChangeSign (u);
    BI_ChangeSign (v);

    result = BI_Compare_Helper (v, u, true);
    /*
       Ach, was muss man oft in bösen,
       nichtfunktionalen Sprachen lesen! :)
       (Genau: Eine Zustandsvariable wird gebraucht.)
    */
    return result;
  }

  /* beide Zahlen sind positiv */

  if (signsChanged) {
    BI_ChangeSign (u);
    BI_ChangeSign (v);
  }  /*
       Zahlen beide negativ?
       Dann Funktion zu primitiv!
       Dreh das Vorzeichen bei beiden,
       dann vergleiche unbescheiden
       ihre Absolutbeträge!
       Spring noch nicht aus der Funktion!
       Sondern nutze Rekursion,
       um sodann bei beiden Zahlen
       das Vorzeichen erneut zu drehen,
       denn tust du's nicht,
       dann wirst du sehen:
       compare ist nicht idempotent,
       die Fehler zeigen sich latent.
     */

  if (u->length < v->length) {
    return -1;
  }

  if (u->length > v->length) {
    return +1;
  }

  /* beide Zahlen sind positiv und gleich lang */

  for (i = u->length-1; i >= 0; i--) {

    Digit du = u->digits[i];
    Digit dv = v->digits[i];

    if (du != dv) {
      return (du > dv ? 1 : -1);
    }
  }

  /* beide Zahlen sind positiv und gleich lang
     und haben an jeder Stelle dieselbe Ziffer

     ===> beide Zahlen sind gleich

  */

  return 0;
}


int BI_CompareInt (BigInteger b, int n) {

  int result = -2;
  BigInteger c;

  if (! BI_isValid (b)) {
    BOOM ("Invalid BigInteger\n");
    return -2;
  }

  c = BI_Create (1);

  BI_SetSmallValue (c, n);
  result = BI_Compare (b, c);

  BI_Destroy (c);

  return result;
}


boolean BI_isGreaterThan (BigInteger u, BigInteger v) {

  if (! BI_isValid (u)) {
    BOOM ("First argument is invalid\n");
    return false;
  }

  if (! BI_isValid (v)) {
    BOOM ("Second argument is invalid\n");
    return false;
  }

  return BI_Compare (u, v) > 0;
}


boolean BI_isLessThan (BigInteger u, BigInteger v) {

  if (! BI_isValid (u)) {
    BOOM ("First argument is invalid\n");
    return false;
  }

  if (! BI_isValid (v)) {
    BOOM ("Second argument is invalid\n");
    return false;
  }

  return BI_Compare (u, v) < 0;
}


boolean BI_isGreaterOrEqual (BigInteger u, BigInteger v) {

  if (! BI_isValid (u)) {
    BOOM ("First argument is invalid\n");
    return false;
  }

  if (! BI_isValid (v)) {
    BOOM ("Second argument is invalid\n");
    return false;
  }

  return BI_Compare (u, v) >= 0;
}


boolean BI_isLessOrEqual (BigInteger u, BigInteger v) {

  if (! BI_isValid (u)) {
    BOOM ("First argument is invalid\n");
    return false;
  }

  if (! BI_isValid (v)) {
    BOOM ("Second argument is invalid\n");
    return false;
  }

  return BI_Compare (u, v) <= 0;
}


int BI_CompareToZero (BigInteger u) {

  if (! BI_isValid (u)) {
    BOOM ("Invalid BigInteger\n");
    return -2;
  }

  return BI_Compare (u, ZERO);
}


boolean BI_isZero (BigInteger u) {

  if (! BI_isValid (u)) {
    BOOM ("Invalid BigInteger\n");
    return false;
  }

  return BI_Compare (u, ZERO) == 0;
}


BigInteger BI_Abs (BigInteger u) {

  BigInteger result;

  if (! BI_isValid (u)) {
    BOOM ("Invalid BigInteger\n");
    return INVALID;
  }

  result = BI_Clone (u);

  if (BI_isNegative (u)) {
    BI_ChangeSign (result);
  }

  return result;
}


boolean BI_isNegative (BigInteger u) {

  if (! BI_isValid (u)) {
    BOOM ("Invalid BigInteger\n");
    return false;
  }

  return BI_Compare (u, ZERO) < 0;
}


boolean BI_isNonNegative (BigInteger u) {

  if (! BI_isValid (u)) {
    BOOM ("Invalid BigInteger\n");
    return false;
  }

  return BI_Compare (u, ZERO) >= 0;
}


boolean BI_isPositive (BigInteger u) {

  if (! BI_isValid (u)) {
    BOOM ("Invalid BigInteger\n");
    return false;
  }

  return BI_Compare (u, ZERO) > 0;
}


boolean BI_isNonPositive (BigInteger u) {

  if (! BI_isValid (u)) {
    BOOM ("Invalid BigInteger\n");
    return false;
  }

  return BI_Compare (u, ZERO) <= 0;
}


boolean BI_isEqual (BigInteger u, BigInteger v) {

  if (! BI_isValid (u)) {
    BOOM ("First argument is invalid\n");
    return false;
  }

  if (! BI_isValid (v)) {
    BOOM ("Second argument is invalid\n");
    return false;
  }

  return BI_Compare (u, v) == 0;
}


BigInteger BI_Maximum (BigInteger u, BigInteger v) {

  if (! BI_isValid (u)) {
    BOOM ("First argument is invalid\n");
    return false;
  }

  if (! BI_isValid (v)) {
    BOOM ("Second argument is invalid\n");
    return false;
  }

  if (BI_isGreaterOrEqual(u, v)) {
    return u;
  } else {
    return v;
  }
}


BigInteger BI_Minimum (BigInteger u, BigInteger v) {

  if (! BI_isValid (u)) {
    BOOM ("First argument is invalid\n");
    return false;
  }

  if (! BI_isValid (v)) {
    BOOM ("Second argument is invalid\n");
    return false;
  }

  if (BI_isLessOrEqual(u, v)) {
    return u;
  } else {
    return v;
  }
}


boolean BI_isIdentical (BigInteger u, BigInteger v) {
  return u == v;
}
