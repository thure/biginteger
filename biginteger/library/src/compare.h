/*
  Vergleichsoperationen
*/

#ifndef BIGINT_COMPARE_H

#define BIGINT_COMPARE_H

#include "datatypes.h"


/*
  Gibt -1, 0, +1 zurück, je nachdem u < v, u == v oder u > v ist.
 */
int BI_Compare (BigInteger u, BigInteger v);


/*
  Gibt -1, 0, +1 zurück, je nachdem b < n, b == n oder b > n ist.
 */
int BI_CompareInt (BigInteger b, int n);


/*
  Gibt genau dann true zurück, wenn u > v ist.
 */
boolean BI_isGreaterThan (BigInteger u, BigInteger v);


/*
  Gibt genau dann true zurück, wenn u < v ist.
 */
boolean BI_isLessThan (BigInteger u, BigInteger v);


/*
  Gibt genau dann true zurück, wenn u >= v ist.
 */
boolean BI_isGreaterOrEqual (BigInteger u, BigInteger v);


/*
  Gibt genau dann true zurück, wenn u <= v ist.
 */
boolean BI_isLessOrEqual (BigInteger u, BigInteger v);


/*
  Gibt -1, 0, +1 zurück, je nachdem u < 0, u == 0 oder u > 0 ist.
 */
int BI_CompareToZero (BigInteger u);


/*
  Gibt genau dann true zurück, wenn u == 0 ist.
 */
boolean BI_isZero (BigInteger u);


/*
  Ermittelt den Betrag von u.
 */
BigInteger BI_Abs (BigInteger u);


/*
  Gibt genau dann true zurück, wenn u < 0 ist.
 */
boolean BI_isNegative (BigInteger u);


/*
  Gibt genau dann true zurück, wenn u >= 0 ist.
 */
boolean BI_isNonNegative (BigInteger u);


/*
  Gibt genau dann true zurück, wenn u > 0 ist.
 */
boolean BI_isPositive (BigInteger u);


/*
  Gibt genau dann true zurück, wenn u <= 0 ist.
 */
boolean BI_isNonPositive (BigInteger u);


/*
  Gibt genau dann true zurück, wenn u und v denselben Wert haben.
  Das schließt den Fall ein, dass u und v auf denselben bigint
  zeigen.
  Dieser Test kann zum Beispiel genutzt werden, um Speicher
  einzusparen, wenn bekannt ist, dass die Werte zweier BigIntegers
  sich über längere Zeit nicht ändern.
 */
boolean BI_isEqual (BigInteger u, BigInteger v);


/*
  Gibt "den" größeren der BigIntegers u und v zurück.
 */
BigInteger BI_Maximum (BigInteger u, BigInteger v);


/*
  Gibt "den" kleineren der BigIntegers u und v zurück.
 */
BigInteger BI_Minimum (BigInteger u, BigInteger v);


/*
  Gibt genau dann true zurück, wenn u und v auf denselben bigint zeigen.
  Das ist ein strengerer Test als BI_isEqual.
 */
boolean BI_isIdentical (BigInteger u, BigInteger v);


/* noch mehr Ideen? */

#endif
