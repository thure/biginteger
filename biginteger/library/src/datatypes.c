/*
  Definition der Datentypen;
  Erzeugung und Vernichtung von BigIntegers;
  Setzen und Auslesen von Werten
*/


#include "datatypes.h"
#include "ioterm.h"     /* debug only? */
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h> /* für memset */
#include <ctype.h>


static boolean check_length (NumberLength length);
static void    BI_RecomputeEffectiveLength (BigInteger b);
static boolean parseString (const char* string, StringParseData* spp);
static boolean canSetDecStringValueAlt (BigInteger b,
                                        char* string,
                                        StringParseData* parsingResults);

static boolean check_length (NumberLength length) {
  if (length <= 0) {
    BOOM ("Invalid initialiser: "
          "BigInteger without digits \n");
    return false;
  }

  if (length >= LONG_MAX - 5) {
    BOOM ("Invalid initialiser: "
          "BigInteger with too many digits \n");
    return false;
  }
  return true;
}

/* Bekommt eine natürliche Zahl length übergeben, fordert ausreichend
   Speicher an, um length char-Werte zusammenhängend aufzunehmen,
   initialisiert alle Komponenten mit 0, und steckt einen Zeiger
   auf diesen Speicherblock in ein neues bigint-Objekt.
   Liefert einen Zeiger auf das bigint-Objekt zurück.
*/
BigInteger BI_Create (NumberLength length) {

  /* http://stackoverflow.com/questions/2702928/returning-a-struct-pointer
     2015-05-08
  */

  BigInteger p = INVALID; /* Zeiger auf bigint-Struktur */
  StringOfDigits digits = NULL; /* Zeiger auf die Ziffern */

  if (!check_length (length)) {
    return INVALID;
  }

  digits = calloc (length, sizeof(Digit));

  /*
    malloc plus Schleife zum Nullsetzen braucht bei 1098765432 Elementen
    8.3 Sekunden.
    calloc allein braucht bei ebensovielen Elementen 1.7 Sekunden.
    (gcc-Optionen: -ansi -Wall -Wextra -Wfatal-errors -pedantic -ggdb3)
  */

  if (digits == NULL) {
    BOOM ("Unable to create BigInteger: "
          "No room for the digits\n");
    return INVALID;
  }

  p = malloc (sizeof(bigint));
  if (p == NULL) {
    BOOM ("Failed to allocate mem for ptr to bigint!\n");
    free(digits);
    return INVALID;
  }

  p->length    = length;
  p->effLength = 1;
  p->sign      = '+';
  p->digits    = digits;

  return p;
}


BigInteger BI_CreateFromDecString (char* string) {
  BigInteger result = BI_Create (1);
  BI_SetDecStringValue (result, string);
  return result;
}


void BI_Destroy (BigInteger b) {
  free (b->digits);
  free (b);
}


BigInteger BI_Clone (BigInteger b) {

  BigInteger result;
  DigitPosition i = -2;

  if (! BI_isValid (b)) {
    BOOM ("Invalid BigInteger\n");
    return INVALID;
  }

  result = BI_Create (b->length);

  result->effLength = b->effLength;
  result->sign = b->sign;

  for (i = b->length-1; i >= 0; i--) {
    BI_SetDigit (result, i, b->digits[i]);
  }
  return result;
}


void BI_CopyValueAlt (BigInteger src,
                      BigInteger dest,
                      CopyStrategy strategy) {

  if (! BI_isValid (src)) {
    BOOM ("src is invalid\n");
    return;
  }

  if (! BI_isValid (dest)) {
    BOOM ("dest is invalid\n");
    return;
  }

  if (dest->length < src->length) {

    /* Im Ziel ist zu wenig Platz */

    DigitPosition i = -2;

    switch (strategy) {

    case BAILOUT:
      return;
      break;

    case FORCE_OVERWRITE:
      dest->sign = src->sign;
      for (i = dest->length-1; i >= 0; i--) {
        BI_SetDigit (dest, i, src->digits[i]);
      }
      break;

    case EXTEND:
      BI_ChangeLength (dest, src->length);
      dest->sign = src->sign;
      for (i = dest->length-1; i >= 0; i--) {
        BI_SetDigit (dest, i, src->digits[i]);
      }
      break;

    default:
      BOOM ("Invalid CopyStrategy. Will do nothing.\n");
      break;
    }

  } else {

    /* Im Ziel ist genug Platz */

    DigitPosition i = -2;
    long int lengthDiff = dest->length - src->length;

    dest->sign = src->sign;

    for (i = 1; i <= lengthDiff; i++) {
      BI_SetDigit (dest, dest->length - i, 0);
    } /* Führende Nullen erzeugen */

    for (i = src->length-1; i >= 0; i--) {
      BI_SetDigit (dest, i, src->digits[i]);
    } /* eigentlichen Wert in die niederwertigen Stellen von dest kopieren */
  }
  BI_RecomputeEffectiveLength (dest);
}


void BI_CopyValue (BigInteger src, BigInteger dest) {
  BI_CopyValueAlt (src, dest, EXTEND);
}


boolean BI_isValid (BigInteger b) {

  /* Viele Funktionen, etwa BI_SumDigitsMod10*, haben an sich
     lineare Laufzeit in der Anzahl der Ziffern, und das Vorschalten
     von BI_isValid führt zu quadratischem Aufwand, der durch
     die Überprüfung, ob sich im Ziffernarray tatsächlich
     ausschließlich Ziffern befinden, verursacht wird.

     Ein Ansatz, diese Überprüfung zu umgehen, besteht darin,
     niemals das Ziffernarray direkt zu modifizieren, sondern stets
     die Funktion BI_SetDigit zu benutzen, und zwar nicht nur
     in den Testroutinen und später in den Produktivanwendungen,
     sondern auch in der Bibliothek selbst, etwa bei der
     Implementierung der eigentlichen Arithmetik.
     Daher wurde die entsprechende Schleife entfernt,
     es sei denn der Debug-Modus ist aktiv.
  */

  if (b == NULL) {

    BOOM ("b == NULL\n");
    return false;

  }

  if (b->length < 1) {
    BOOM ("b->length < 1\n");
    return false;
  }

  if (b->sign != '+' && b->sign != '-') {
    BOOM ("b->sign != \'+\' && b->sign != \'-\'\n");
    return false;
  }

  if (b->digits == NULL) {
    BOOM ("b->digits == NULL\n");
    return false;
  }

  if (b->effLength > b->length) {
    BOOM ("b->effLength > b->length\n");
    return false;
  }

  /* Gibt es noch mehr Fehlerfälle?  */

  return true;
}

Digit BI_GetDigit (BigInteger b, DigitPosition i) {

  if (! BI_isValid (b)) {
    BOOM ("Invalid BigInteger\n");
    return EOF;
  }

  if (i+1 > b->length || i < 0) {
    BOOM2 ("Invalid digit position: %ld\n", i);
    return EOF;
  }

  return b->digits[i];
}

void BI_SetDigit (BigInteger b, DigitPosition i, Digit d) {

  if (! BI_isValid (b)) {
    BOOM ("Invalid BigInteger\n");
    return;
  }

  if (i+1 > b->length || i < 0) {
    BOOM2 ("Invalid digit position: %ld\n", i);
    return;
  }

  b->digits[i] = d;
}


static int countDigits (long int n) {
  /*  http://stackoverflow.com/a/1068865,  2015-05-06  */
  int result = 0;
  if (n < 0) n = -n;
  while (n != 0) {
    n /= 10;
    result++;
  }
  return result;
}


boolean BI_ChangeLength (BigInteger b,
                         NumberLength newLength) {

  /* http://stackoverflow.com/a/9142813,  2015-05-06 */

  StringOfDigits newDigits = NULL;
  long int lengthDiff      = 0; /* neue Länge minus alte Länge */
  long int i               = 0; /* Schleifenzähler */
  long int oldLength       = 0;

  if (! BI_isValid (b)) {
    BOOM ("Invalid BigInteger\n");
    return false;
  }

#ifdef DEBUG2
  printf ("Wert vor der Längenänderung:  ");
  BI_PrintlnRaw (b);
#endif

  if (newLength == b->length) {
    return true;
  }

  if (newLength < b->effLength) {
    BOOM2 ("new length (%ld) is too small (needs %ld)\n", newLength, b->effLength);
    return false;
  }

  oldLength  = b->length;
  lengthDiff = newLength - oldLength;

#ifdef DEBUG2
  printf ("Ändere Länge von %ld auf %ld Ziffern\n", b->length, newLength);
  printf ("D.h. %s um %ld Stellen\n\n", lengthDiff >= 0 ? "Verlängerung" : "Verkürzung",
          lengthDiff >= 0 ? lengthDiff : -lengthDiff);
#endif

  newDigits = realloc(b->digits, newLength);

  if (newDigits == NULL) {
    BOOM ("realloc failed\n");
    return false;
  }
  b->digits = newDigits;
  b->length = newLength;

  if (b->isAComplement) {
    
    for (i = 1; i <= lengthDiff; i++) {
      memset (b->digits + oldLength, 9, lengthDiff);
      /* Führende Neunen erzeugen */
    }

  } else {
    
    for (i = 1; i <= lengthDiff; i++) {
      memset (b->digits + oldLength, 0, lengthDiff);
      /* Führende Nullen erzeugen */
    }
  }

#ifdef DEBUG2
  printf ("Wert nach der Längenänderung: ");
  BI_PrintlnRaw (b);
  printf ("\n\n");
#endif
  
  return true;
}


void BI_SetSmallValue (BigInteger b, long int v) {

  int vdigits = countDigits(v);
  DigitPosition i = -2;

  if (! BI_isValid (b)) {
    BOOM ("Invalid BigInteger\n");
    return;
  }

  if (v < 0) {
    b->sign = '-';
    v = -v;
  } else {
    b->sign = '+';
  }

  if (b->length < vdigits) {
    BI_ChangeLength (b, vdigits);
  }

  for (i = 0; i < b->length; i++) {
    memset (b->digits, 0, b->length);
  } /* Das geht mit memset etwa 10% schneller als mit
       BI_SetDigit (b, i, 0). Und ja, das bricht die Regel,
       ausschließlich mit BI_SetDigit zu arbeiten. Da aber
       offensichtlich nur Nullen ins Array geschrieben werden,
       sehen wir darüber hinweg.
    */

  i = 0;
  while (v != 0) {
    BI_SetDigit(b, i, v % 10);
    v = v / 10;
    i++;
  }
  /* BI_RecomputeEffectiveLength (b); */
  BI_Canonicalise (b);
#ifdef DEBUG2
  printf ("SetSmallValue: ");
  BI_PrintlnRaw(b);
#endif
}


void BI_SetDecStringValueAlt (BigInteger b,
                              char* string,
                              CopyStrategy strategy) {
  NumberLength     dstLen;
  NumberLength     srcLen;
  StringParseData* parsingResults = malloc(sizeof (StringParseData));

  if (! canSetDecStringValueAlt (b, string, parsingResults)) {
    free (parsingResults);
    return;
  }

  srcLen = parsingResults->numberOfEffDigits;
  dstLen = b->length;

  if (dstLen < srcLen) { /* Im Ziel ist nicht genug Platz */

    DigitPosition i = -2;
    long int srcIndex;

    switch (strategy) {

    case BAILOUT:
      return;
      break;

    case FORCE_OVERWRITE:
      b->sign = parsingResults->sign;
      srcIndex = parsingResults->startOfEffDigits;
      for (i = b->length - 1; i >= 0; i--, srcIndex++) {
        for (; ! isdigit(string[srcIndex]); srcIndex++)
          ;
        BI_SetDigit (b, i, string[srcIndex] - '0');
      }
      break;

    case EXTEND:
      BI_ChangeLength (b, srcLen);
      b->sign = parsingResults->sign;
      srcIndex = parsingResults->startOfEffDigits;
      for (i = srcLen-1; i >= 0; i--, srcIndex++) {
        for (; ! isdigit(string[srcIndex]); srcIndex++)
          ;
        BI_SetDigit (b, i, string[srcIndex] - '0');
      }
      break;

    default:
      BOOM ("Invalid CopyStrategy. Will do nothing.\n");
      break;
    }
  }

  else { /* Im Ziel ist genug Platz */

    DigitPosition i = -2;
    long int srcIndex;
    b->sign = parsingResults->sign;
    for (i = srcLen; i < dstLen; i++) {
      BI_SetDigit (b, i, 0);
    }
    srcIndex = parsingResults->startOfEffDigits;
    for (i = srcLen-1; i >= 0; i--, srcIndex++) {
      for (; ! isdigit(string[srcIndex]); srcIndex++)
        ;
      BI_SetDigit (b, i, string[srcIndex] - '0');
    }
  }
  BI_RecomputeEffectiveLength (b);
}


void BI_SetDecStringValue (BigInteger b, char* string) {
  BI_SetDecStringValueAlt (b, string, EXTEND);
}


/*
  Stellt fest, ob der Funktion BI_SetStringValueAlt
  gültige Parameter übergeben wurden.
*/
static boolean canSetDecStringValueAlt (BigInteger b,
                                        char* string,
                                        StringParseData* parsingResults) {
  if (parsingResults == NULL) {
    BOOM ("no room to store input string properties\n");
    return false;
  }

  if (! BI_isValid (b)) {
    BOOM ("Invalid BigInteger\n");
    return false;
  }
  
  if (string == NULL) {
    BOOM ("no input string specified\n");
    return false;
  }
  
  if (! parseString (string, parsingResults)) {
    BOOM ("input string is not an integer\n");
    return false;
  }
  return true;
}


/*
  Gibt genau dann true zurück, wenn string auf einen String zeigt, der
  als Dezimalzahl mit optionalem Vorzeichen interpretiert werden
  kann. Leerzeichen sind, auch zwischen den Ziffern, erlaubt.
  Andere Whitespace-Zeichen sind nicht erlaubt.
*/
static boolean parseString (const char* string, StringParseData* spp) {
  
  int i = -2;
  boolean firstEffDigitSeen = false; /* erste Ziffer gesehen, die
                                        keine führende Null ist */
  
  if (string == NULL) {
    BOOM ("no input string specified\n");
    return false;
  }
  
  if (spp == NULL) {
    BOOM ("no room to store parsing results\n");
    return false;
  }
  
  
  if (string[0] == '\0') {
    BOOM ("input string is empty\n");
    return false;
  }
  
  spp->sign = '\\';
  spp->startOfEffDigits = 0;
  spp->numberOfEffDigits = 0;
  
  for (i = 0; string[i] == ' '; i++) {
    /* Leerzeichen überspringen */
  }
  
  /* jetzt KANN ein Pluszeichen oder ein Minuszeichen folgen */
  if (string[i] == '-') {
    spp->sign = '-';
    spp->startOfEffDigits++;
    i++;
  } else {
    spp->sign = '+';
    if (string[i] == '+') {
      spp->startOfEffDigits++;
      i++;
    }
  }
  
  /* jetzt dürfen nur noch Ziffern und Leerzeichen folgen */
  for (; string[i] == '0' || string[i] == ' '; i++) {
    /* führende Nullen und Leerzeichen überspringen */
  }
  
  /* Sofern 0 oder -0 eingegeben wurde, ist die Null übersprungen
     worden.
  */
  
  if (string[i] == '\0') {
    spp->numberOfEffDigits = 1;
    spp->sign = '+'; /* die Null soll das positive Vorzeichen haben */
    return true;
  }
  
  for (; string[i] != '\0'; i++) {
    
    if ((! isdigit(string[i]) && string[i] != ' ')) {
      BOOM2 ("Pos. %d: weder Ziffer noch Leerzeichen\n", i);
      return false;
    }
    
    if ((! firstEffDigitSeen) && isdigit(string[i])) {
      firstEffDigitSeen = true;
      spp->startOfEffDigits = i;
    }
    if (isdigit(string[i])) {
      spp->numberOfEffDigits++;
    }
  }
  
  /* jetzt zeigt i auf das abschließende Nullzeichen */
  /* spp->numberOfEffDigits = i - spp->startOfEffDigits; */

  return true;
}


void BI_Canonicalise (BigInteger b) {

  if (! BI_isValid (b)) {
    BOOM ("Invalid BigInteger\n");
    return;
  }

  BI_RecomputeEffectiveLength (b);

  BI_ChangeLength (b, b->effLength);

  if (b->effLength == 1 && b->digits[0] == 0) {
    b->sign = '+';
  }
}

static void BI_RecomputeEffectiveLength (BigInteger b) {

  NumberLength efflen = b->length;

  if (! BI_isValid (b)) {
    BOOM ("Invalid BigInteger\n");
    return;
  }

  for (efflen = b->length;
       efflen > 1 && b->digits[efflen-1] == 0;
       efflen--) { /* führende Nullen nicht zählen */
  }
  b->effLength = efflen;
}
