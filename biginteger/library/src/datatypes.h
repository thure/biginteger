/*
  Definition der Datentypen;
  Erzeugung und Vernichtung von BigIntegers;
  Setzen und Auslesen von Werten
*/

#ifndef BIGINT_DATATYPES_H

#define BIGINT_DATATYPES_H

#include <stdlib.h>
#include <limits.h>

/* Fehlerbehandlung.
   Siehe auch
   - https://gcc.gnu.org/onlinedocs/cpp/Standard-Predefined-Macros.html
   - https://gcc.gnu.org/onlinedocs/cpp/Variadic-Macros.html
*/

#define BOOM(msg)                              \
  do { fprintf (stderr, "\n%s: %s\n   at %s, " \
                "line %d.\n\n", __func__, msg, \
                __FILE__, __LINE__);           \
  /* exit (5);   */                                  \
  } while (0)

#define BOOM2(format, ...)                   \
  do { BOOM(format);                         \
    fprintf (stderr, format, ##__VA_ARGS__); \
    exit (5);                                \
  } while (0)

typedef char boolean;
#define false (1 != 1)
#define true  (1 == 1)
/*
  Ein Aufzählungstyp hilft hier nicht weiter, genausowenig wie die
  C99-Makros. Beide verhindern nicht, dass man einem boolean Werte
  zuweist, die weder 0 noch 1 sind.
  TODO: Das dürfte aber mit dem gcc-Flag  -fshort-enums  machbar sein.
 */


#define B CHAR_BIT
typedef unsigned char Digit;      /* Eine Ziffer zur Basis 2^B */
typedef Digit* StringOfDigits;    /* Zeiger auf den Beginn des Ziffernarrays */
typedef long int DigitPosition;   /* Ziffern eines BigIntegers adressieren */
typedef long int NumberLength;
typedef signed char radix;        /* Basis eines Stellenwertsystems
                                     zur Ein-/Ausgabe */

/* char* ist ein Zeiger auf ein char.
   Indem wir an dieser Stelle ein ganzes Array von chars ablegen,
   können wir char* als einen Zeiger auf ein char-Array auffassen.
   Indem wir in jedem Element des Arrays eine Dezimalziffer ablegen,
   ist ein char-Array ein Array von Dezimalziffern, also eine Darstellung
   einer natürlichen Zahl.
*/

typedef struct {

  NumberLength     length; /* Anzahl der Ziffern, die derzeit
                              gespeichert werden können, > 0 */

  NumberLength     effLength; /* Anzahl der Ziffern, die tatsächlich
                                 benötigt werden, um den aktuellen
                                 Wert zu speichern */

  char             sign;   /* Vorzeichen */

  boolean          isAComplement; /* gibt an, ob die Zahl das
                                     16-Komplement einer anderen
                                     Zahl darstellt */

  StringOfDigits   digits;  /* Zeiger auf die die Ziffern */

} bigint;

typedef struct {

  char sign;              /* Vorzeichen */

  int  startOfEffDigits;  /* Position der höchstwertigen Ziffer */

  int  numberOfEffDigits; /* Anzahl der Ziffern ohne führende Nullen */

} StringParseData;

typedef bigint* BigInteger;


static bigint invalid = { -1, -1, '\\', false, NULL };
static const BigInteger INVALID = &invalid;
/*  http://stackoverflow.com/a/2328715,  2015-05-06 */

static const int zerodigits[1] = {0};
static bigint zero = { 1, 1, '+', false, (Digit*)zerodigits };
static const BigInteger ZERO = &zero;

static const int onedigits[1] = {1};
static bigint one = { 1, 1, '+', false, (Digit*)onedigits };
static const BigInteger ONEE = &one;



/*
  Legt fest, was beim Kopieren eines BigIntegers in einen anderen
  oder beim Zuweisen eines Werts aus einem C-String
  geschehen soll, wenn der Platz im Ziel nicht ausreicht.
  (Funktion BI_CopyValue)

  BAILOUT:         Do nothing.

  FORCE_OVERWRITE: Overwrite all of dest's digits with those from src,
                   discarding the high-order digits of src.
                   If b is positive, this amounts to a
                   modulo 16^k computation for some integer k.

  EXTEND:          Extend dest to match src's length.
*/
typedef enum { BAILOUT = 0, FORCE_OVERWRITE, EXTEND } CopyStrategy;


/*
  Erzeugt ein neues BigInteger-Objekt.
*/
BigInteger BI_Create (NumberLength length);


/*
  Erzeugt ein neues BigInteger-Objekt und weist ihm einen Anfangswert zu.
  string muss ein C-String sein, in dem Dezimal(!)ziffern in atoi-kompatibler
  Reihenfoge gespeichert sind.
*/
BigInteger BI_CreateFromDecString (char* string);


/*
  Erzeugt ein neues BigInteger-Objekt und weist ihm einen Anfangswert zu.
  string muss ein C-String sein, in dem Ziffern zur Basis b
  in atoi-kompatibler Reihenfoge gespeichert sind.
*/
BigInteger BI_CreateFromString (char* string, radix b);



/*
  Gibt den Speicher wieder frei, den der BigInteger b belegt.
*/
void BI_Destroy (BigInteger b);


/*
  Erstellt einen neuen BigInteger mit demselben Wert wie b.
  Dieser muss wieder freigegeben werden, sobald er nicht mehr
  gebraucht wird.
*/
BigInteger BI_Clone (BigInteger b);


/*
  Kopiert den Wert von src in dest.
*/
void BI_CopyValue (BigInteger src, BigInteger dest);


/*
  Kopiert den Wert von src in dest.
  Je nach Wert von strategy wird, sofern der Platz in b nicht
  ausreicht, die Funktion ergebnislos abgebrochen, die maximale Anzahl
  an Stellen kopiert oder die Länge von b erhöht.
*/
void BI_CopyValueAlt (BigInteger src,
                            BigInteger dest,
                            CopyStrategy strategy);

/*
  Stellt fest, ob ein BigInteger-Objekt gültige Daten enthält.
*/
boolean BI_isValid(BigInteger);


/*
  Gibt eine bestimmte Ziffer (zur Basis 2^B) des BigIntegers zurück.
*/
Digit BI_GetDigit (BigInteger b, DigitPosition i);


/*
  Setzt die Ziffer i auf den Wert d.
*/
void BI_SetDigit (BigInteger b, DigitPosition i, Digit d);


/*
  Erweitert den BigInteger b, um darin bis zu newLength Ziffern
  abzulegen. Wenn eine Subtraktion durchgeführt werden soll,
  werden führende Stellen mit 9 aufgefüllt, sonst mit 0.
  Gibt genau dann true zurück, wenn die Längenänderung unnötig oder
  erfolgreich ist.  
*/
boolean BI_ChangeLength (BigInteger b, NumberLength newLength);


/*
  Speichert den Wert v im BigInteger b.
*/
void BI_SetSmallValue (BigInteger b, long int v);


/*
  Speichert den Wert, den der String str darstellt, im BigInteger b.
  str muss auf einen gültigen, also insbesondere '\0'-terminierten,
  C-String zeigen, der Dezimal(!)ziffern in atoi-kompatibler
  Reihenfolge enthält.
*/
void BI_SetDecStringValue (BigInteger b, char* string);


/*
  Speichert den Wert, den der String str darstellt, im BigInteger b.
  str muss auf einen gültigen, also insbesondere '\0'-terminierten,
  C-String zeigen, der Dezimal(!)ziffern in atoi-kompatibler
  Reihenfolge enthält.
  Je nach Wert von strategy wird, sofern der Platz in b nicht
  ausreicht, die Funktion ergebnislos abgebrochen, die maximale Anzahl
  an Stellen kopiert oder die Länge von b erhöht.
*/
void BI_SetDecStringValueAlt (BigInteger b,
                                  char* string,
                                  CopyStrategy strategy);


/*
  Bringt den BigInteger b in kanonische Form (d.h. minimale Länge,
  keine führenden Nullen, 0 hat das positive Vorzeichen, ...)
*/
void BI_Canonicalise (BigInteger b);




#endif
