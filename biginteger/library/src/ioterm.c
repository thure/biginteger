/*
  Ein- und Ausgabe auf den Standardgeräten
*/

#include "ioterm.h"
#include "datatypes.h"
#include <stdio.h>
#include <stddef.h> /* für ptrdiff_t */

void BI_PrintAllRawDigits (BigInteger b) {

  DigitPosition i = -2;

  if (! BI_isValid (b)) {
    BOOM ("Invalid BigInteger\n");
    return;
  }

  if (b->sign == '-') {
    printf ("-");
  }

  for (i = b->length-1; i >= 0; i--) {
    printf ("%3x", b->digits[i]);
  }
}


void BI_PrintlnAllRawDigits (BigInteger b) {
  BI_PrintAllRawDigits (b);
  printf ("\n");
}


void BI_PrintRaw (BigInteger b) {
  DigitPosition i = -2;

  if (! BI_isValid(b)) {
    BOOM ("Invalid BigInteger\n");
    return;
  }

  if (b->sign == '-') {
    printf ("-");
  }

  for (i=b->effLength-1; i>= 0; i--) {
    printf (" %3x", b->digits[i]);
    /* if (i>0) printf ("."); */
  }

}


void BI_PrintlnRaw (BigInteger b) {
  BI_PrintRaw (b);
  printf ("\n");
}


void BI_PrintAddresses (BigInteger b) {

  Digit* firstdigit = b->digits;
  Digit* lastdigit  = b->digits + b->length - 1;
  ptrdiff_t howmany = lastdigit-firstdigit+1;

  printf ("\n");
  printf ("&b            = %p\n Adresse des Zeigers auf bigint\n\n",
          (void*)(&b));
  printf (" b            = %p\n Adresse des bigints\n\n", (void*)(b));

  printf ("&b->length    = %p\n Adresse der Länge des bigints\n\n",
          (void*)(&b->length));
  printf ("&b->effLength = %p\n Adresse der eff. Länge des bigints\n\n",
          (void*)(&b->effLength));
  printf ("&b->sign      = %p\n Adresse des Vorzeichens des bigints\n\n",
          (void*)(&b->sign));
  printf ("&b->digits    = %p\n Adresse des Zeigers auf die Ziffern\n\n",
          (void*)(&b->digits));
  printf ("b->digits     = %p ... %p\n Adresse der Ziffern des bigints\n\n",
          (void*)firstdigit,    (void*)lastdigit);

  printf ("Die %ld Ziffern belegen %td Bytes\n\n", b->length,
          howmany);
  /*
    howmany gibt die Anzahl der Ziffern an. Da jede Ziffer aber genau
    1 char belegt und sizeof (char) == 1 gilt, ist dies genau die
    Anzahl der belegten Bytes.
  */

  printf ("b->length     = %ld       Länge des bigints\n",
          b->length);
  printf ("b->effLength  = %ld       eff. Länge des bigints\n",
          b->effLength);
  printf ("b->sign       = %c       Vorzeichen des bigints\n",
          b->sign);
  printf ("\n");

  printf ("Höchst- und niedrigstwertige Ziffern des bigints: ");
  if (b->sign == '-') {
    printf ("-");
  }
  if (b->length <= 25) {
    BI_PrintlnAllRawDigits (b);
  } else {
    DigitPosition i = -2;
    for (i = b->length-1; i >= b->length-10; i--) {
      printf ("%x", b->digits[i]);
    }
    printf ("...");
    for (i = 9; i >= 0; i--) {
      printf ("%x", b->digits[i]);
    }
  }
  printf ("\n");
}


/* NEU */

void BI_PrintRadix (BigInteger n, radix b) {

  DigitPosition i = -2;
  i = i;

  if (! BI_isValid (n)) {
    BOOM ("Invalid BigInteger\n");
    return;
  }

  if (b < 1 || b > 36) {
    BOOM ("Invalid radix\n");
    return;
  }

  if (n->sign == '-') {
    printf ("-");
  }

  b = b;
  BOOM ("Zahl muss umgewandelt und ausgegeben werden!");

}


void BI_PrintlnRadix (BigInteger n, radix b) {

  BI_PrintRadix (n, b);
  printf ("\n");

}


void BI_PrintHex (BigInteger n) {

  BI_PrintRadix(n, 16);
    
}


void BI_PrintlnHex (BigInteger n) {

  BI_PrintlnRadix(n, 16);

}


void BI_PrintDec (BigInteger n) {

  BI_PrintRadix(n, 10);

}


void BI_PrintlnDec (BigInteger n) {

  BI_PrintlnRadix(n, 10);

}
