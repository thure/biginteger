/*
  Ein- und Ausgabe auf den Standardgeräten
*/


#ifndef BIGINT_IOTERM_H

#define BIGINT_IOTERM_H

#include "datatypes.h"


/*
  Gibt alle Ziffern eines BigInteger-Objekts auf der Standardausgabe aus.
  Benutzt die Zahlen 0 ... FF zur Darstellung.
 */
void BI_PrintAllRawDigits (BigInteger b);


/*
  Gibt alle Ziffern eines BigInteger-Objekts auf der Standardausgabe aus,
  gefolgt von einem Zeilenumbruch.
  Benutzt die Zahlen 0 ... FF zur Darstellung.
 */
void BI_PrintlnAllRawDigits (BigInteger b);


/*
  Gibt ein BigInteger-Objekt auf der Standardausgabe aus.
  Benutzt die Zahlen 0 ... FF zur Darstellung.
 */
void BI_PrintRaw (BigInteger b);

/*
  Gibt ein BigInteger-Objekt auf der Standardausgabe aus,
  gefolgt von einem Zeilenumbruch.
  Benutzt die Zahlen 0 ... FF zur Darstellung.
 */
void BI_PrintlnRaw (BigInteger b);


/*
  Gibt das Innenleben eines BigInteger-Objekts auf der Standardausgabe
  aus.
 */
void BI_PrintAddresses (BigInteger b);


/*
  Stellt den BigInteger n zur Basis b dar und gibt ihn auf der
  Standardausgabe aus.
  Es muss 1 <= b <= 36 sein.
 */
void BI_PrintRadix (BigInteger n, radix b);


/*
  Stellt den BigInteger n zur Basis b dar und gibt ihn auf der
  Standardausgabe aus, gefolgt von einem Zeilenumbruch.
  Es muss 1 <= b <= 36 sein.
 */
void BI_PrintlnRadix (BigInteger n, radix b);


/*
  Stellt den BigInteger n zur Basis 16 dar und gibt ihn auf der
  Standardausgabe aus.
 */
void BI_PrintHex (BigInteger n);


/*
  Stellt den BigInteger n zur Basis 16 dar und gibt ihn auf der
  Standardausgabe aus, gefolgt von einem Zeilenumbruch.
 */
void BI_PrintlnHex (BigInteger n);


/*
  Stellt den BigInteger n zur Basis 10 dar und gibt ihn auf der
  Standardausgabe aus.
 */
void BI_PrintDec (BigInteger n);


/*
  Stellt den BigInteger n zur Basis 10 dar und gibt ihn auf der
  Standardausgabe aus, gefolgt von einem Zeilenumbruch.
 */
void BI_PrintlnDec (BigInteger n);


#endif
