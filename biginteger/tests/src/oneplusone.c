#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../../library/src/datatypes.h"
#include "../../library/src/ioterm.h"
#include "../../library/src/addsub.h"

#define HOWMANY 160

BigInteger s;
int i;

int main (void) {

  s = BI_CreateFromDecString ("0");

  BI_PrintlnRaw(s);

  for (i = 0; i < HOWMANY/8; i++) {

    BI_Inc (s);
    BI_Inc (s);
    BI_Inc (s);
    BI_Inc (s);
    BI_Inc (s);
    BI_Inc (s);
    BI_Inc (s);
    BI_Inc (s);
    BI_PrintlnRaw (s);

  }

  for (i = 1; i <= 10; i++) {

    BI_AddTo (s, s);
    BI_SmallIncrement (s, 9);
    BI_PrintlnRaw (s);

  }

  BI_Destroy (s);

  return 0;
}
