#include <stdio.h>
#include <stdlib.h>
#include "../../library/src/datatypes.h"
#include "../../library/src/ioterm.h"

int main (void) {

#define MAXARRAY    20
#define MAXDIGITPOS 10

  int i = -1;
  int j = -1;

  BigInteger arr[MAXARRAY];

  for (i = 0; i < MAXARRAY; i++) {

    arr[i] = BI_Create (MAXDIGITPOS);

    for (j = 0; j < MAXDIGITPOS; j++) {

      BI_SetDigit (arr[i], j, ((i+1)*(j+1)) % 256);

    }

    /* BI_PrintlnAllRawDigits (arr[i]); */
    BI_PrintlnRaw (arr[i]);

    BI_Destroy (arr[i]);

  }

  return 0;

}
