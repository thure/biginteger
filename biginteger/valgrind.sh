#!/bin/bash

usage() {

    echo -e "\nUsage: $0"

}

echo

if [ 0 -ne "$#" ]
then
    usage
    exit 1
fi

cd tests/bin || { echo "cannot chdir to executables"; exit 3; }

for file in *.out; do
    echo "$file"
    valgrind "./${file}"  2>&1 | grep 'ERROR SUMMARY'
    echo; echo
done

echo
